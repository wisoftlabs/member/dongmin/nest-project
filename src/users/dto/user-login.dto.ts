export class UserLoginDto {
    email: string;
    password: string;
    birth: string;
    name: string;
}