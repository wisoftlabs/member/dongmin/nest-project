import {IsDate, IsEmail, IsNotEmpty, IsString, Matches, MaxLength, MinLength,Validate} from 'class-validator';


export class UserDto {
    @IsString()
    @Matches(/^[가-힣]+$/, { message: '이름은 한글만 입력 가능합니다.' })
    @MinLength(2, {message: '이름은 최소 2글자 이상이어야 합니다.'})
    @MaxLength(4, {message: '이름은 최대 4글자까지 가능합니다.'})
    @IsNotEmpty()
    readonly name: string;

    @IsString()
    @IsEmail({}, {message: '유효한 이메일 주소 형식이어야 합니다.'})
    @MaxLength(50)
    @IsNotEmpty()
    readonly email: string;

    @Matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{10,14}$/,
        {
            message: '비밀번호는 영문자, 숫자, 특수문자(@$!%*#?&) 중 적어도 1개씩을 포함해야 합니다'
        },
    )
    readonly password: string;


    @IsNotEmpty()
    @Validate(function (value) {
        if (!/^\d{4}-\d{2}-\d{2}$/.test(value)) {
            return false;
        }

        const date = new Date(value);
        const isValidDate = !isNaN(date.getTime());
        return isValidDate;
    }, { message: '올바르지 않은 생년월일 형식입니다. ex) 2023-08-24' })
    readonly birth: string;
}