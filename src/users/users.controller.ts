import {Controller, Post, Get, Body, HttpException, UseFilters, Param, Query} from '@nestjs/common';
import {UsersService} from "./users.service";
import {HttpExceptionFilter} from "../exceptions/http-exception.filter";
import {UserDto} from "./dto/create-user.dto";
import {UserLoginDto} from "./dto/user-login.dto";
import * as uuid from 'uuid';

@Controller('users')
@UseFilters(HttpExceptionFilter)
export class UsersController {
    constructor(private readonly usersService: UsersService) {
    }

    @Post('/signup')
    async signUp(@Body() body: UserDto): Promise<string> {
        console.log(body);
        await this.usersService.signUp(body);
        return '회원가입에 성공하셨습니다. 이메일로 인증 메일이 발송되었습니다.';
    }

    @Post('/login')
    async logIn(@Body() loginDto: UserLoginDto): Promise<string> {
        await this.usersService.logIn(loginDto);
        return '로그인 성공';
        console.log('로그인 성공');
    }


    @Post('/logout')
    logOut() {
        return '로그아웃';
    }

}
