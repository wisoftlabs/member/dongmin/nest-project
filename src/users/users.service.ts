import {Body, Injectable, UnauthorizedException} from '@nestjs/common';
import {UserDto} from './dto/create-user.dto';
import {User} from './user.schema';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import * as uuid from 'uuid';
import {UserLoginDto} from "./dto/user-login.dto";
import {EmailService} from "../email/email.service";

@Injectable()
export class UsersService {
    constructor(
        @InjectModel(User.name) private readonly userModel: Model<User>,
        private emailService: EmailService) {
    }

    async signUp(body: UserDto) {
        const {name, email, password, birth} = body;

        const isUserExist = await this.userModel.exists({email});
        if (isUserExist) {
            throw new UnauthorizedException('이미 가입된 이메일입니다.');
        }

        const signupVerifyToken = uuid.v1();
        await this.sendMemberJoinEmail(body.email, signupVerifyToken);

        const user = await this.userModel.create({
            email,
            name,
            password,
            birth,
        });

        return user;
    }

    async logIn(@Body() body: UserLoginDto) {
        //const {name, email, password, birth} = body;

        if (body.email && body.password && !body.name && !body.birth) {
            const successLogin = await this.userModel.exists({email: body.email, password: body.password});
            if (!successLogin) {
                throw new UnauthorizedException('이메일과 비밀번호를 다시 입력해주세요.');
            }

        } else {
            // If additional fields are provided, throw an error
            throw new UnauthorizedException('잘못된 로그인 정보입니다.');
        }
    }

    private async sendMemberJoinEmail(email: string, signupVerifyToken: string) {
        await this.emailService.sendMemberJoinVerification(email, signupVerifyToken);
    }
}

