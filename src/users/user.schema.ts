import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";
import {IsEmail, IsNotEmpty, IsString} from "class-validator";

@Schema()
export class User {

    @Prop({
        required: true,
    })
    @IsString()
    @IsNotEmpty()
    name: string;

    @Prop({
        required: true,
        unique: true,
    })
    @IsEmail()
    @IsString()
    @IsNotEmpty()
    email: string;

    @Prop({
        required: true,
    })
    @IsString()
    @IsNotEmpty()
    password: string;

    @Prop({
        required: true,
    })
    @IsNotEmpty()
    birth: string;
}

export const UserSchema = SchemaFactory.createForClass(User);