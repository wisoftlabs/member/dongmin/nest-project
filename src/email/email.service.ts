import {Inject, Injectable} from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import Mail from "nodemailer/lib/mailer";

interface EmailOptions {
    to: string;
    subject: string;
    html: string;
}

@Injectable()
export class EmailService {
    private transporter: Mail;

    constructor() {
        this.transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: '20212025o365@gmail.com',
                pass: 'kzwruptqevwzisde'
            }
        });
    }

    async sendMemberJoinVerification(emailAddress: string, signupVerifyToken: string) {
        const baseUrl = 'http://localhost:8000';

        const url = `${baseUrl}/users/email-verify?signupVerifyToken=${signupVerifyToken}`;

        const mailOptions: EmailOptions = {
            to: emailAddress,
            subject: '가입을 환영합니다!', // 변경된 제목
            html: `
           <div style="background-color: #f2f2f2; padding: 20px;">
    <div style="font-family: Arial, sans-serif; background-color: white; padding: 20px; border-radius: 10px; box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);">
        <h2 style="color: #007bff; text-align: center; margin-bottom: 20px;">가입을 환영합니다!</h2>
        <p style="text-align: center;">가입이 완료되었습니다. 환영합니다!</p>
        <div style="text-align: center; margin-top: 30px;">
            <a href="${url}" style="display: inline-block; background-color: #007bff; color: white; padding: 10px 20px; text-decoration: none; border-radius: 5px;">가입 확인</a>
        </div>
    </div>
</div>

        `,
        };

        return await this.transporter.sendMail(mailOptions);
    }
}
