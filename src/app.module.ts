import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {UsersController} from './users/users.controller';
import {UsersModule} from './users/users.module';
import {MongooseModule} from "@nestjs/mongoose";
import { EmailService } from './email/email.service';
import {ConfigModule} from "@nestjs/config";

@Module({
    imports: [
        UsersModule,
        ConfigModule.forRoot(),
        MongooseModule.forRoot(process.env.MONGO_URI),
    ],
    controllers: [AppController, UsersController],
    providers: [AppService, EmailService],
})
export class AppModule {
}
